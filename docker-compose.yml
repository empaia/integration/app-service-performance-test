services:

  app-service:
    image: registry.gitlab.com/empaia/services/app-service:0.7.17@sha256:98bbbd2a0eb6126cc4aedb87d015e43a69d378dd2babde24be3e384f054d48a8
    environment:
      AS_MDS_URL: http://medical-data-service:8000
      AS_MPS_URL: http://marketplace-service-mock:8000
      AS_ENABLE_TOKEN_VERIFICATION: "False"
      AS_MPS_USE_V1_ROUTES: "False"
      AS_ORGANIZATION_ID: dummy-orga-id
      AS_ENABLE_PROFILING: "True"
      NO_PROXY: "marketplace-service-mock,medical-data-service"
    command:
      - --port=8000
      - --host=0.0.0.0
    ports:
      - 8000:8000

  medical-data-service:
    image: registry.gitlab.com/empaia/services/medical-data-service:0.7.5@sha256:6f07a6c144b60e9b75b7cc1a6685e26580af9165287b9e5cc3ab05066ac28fc2
    command:
      - uvicorn
      - --workers=1
      - --host=0.0.0.0
      - --port=8000
      - medical_data_service.app:app
    environment:
      MDS_CONNECTION_CHUNK_SIZE: 1024000
      MDS_CONNECTION_TIMEOUT: 300
      MDS_CORS_ALLOW_ORIGINS: '["*"]'
      MDS_API_INTEGRATION: medical_data_service.api.integrations.disable_auth:DisableAuth
      MDS_ROOT_PATH: /
      MDS_DISABLE_OPENAPI: "false"
      MDS_JS_URL: http://job-service:8000
      MDS_AS_URL: http://annotation-service:8000
      MDS_CDS_URL: http://clinical-data-service:8000
      MDS_WS_URL: http://wsi-service:8080
      MDS_SMS_URL: http://storage-mapper-service:8000
      MDS_ES_URL: http://examination-service:8000
      MDS_ENABLE_PROFILING: "true"
      NO_PROXY: "job-service,annotation-service"
    ports:
      - 8001:8000
    depends_on:
      - annotation-service
      - job-service

  job-service:
    image: registry.gitlab.com/empaia/services/job-service:0.7.2@sha256:615c90ce7b353208da06e5deefcebcd74c4705e976123b9b0d2a298505e815e4
    command: run.sh --host=0.0.0.0 --port=8000 --workers=1
    environment:
      JS_ROOT_PATH: /
      JS_RSA_KEYS_DIRECTORY: /app/rsa
      JS_DB_HOST: mds-db
      JS_DB_PORT: 5432
      JS_DB: mds
      JS_DB_USERNAME: empaia_test
      JS_DB_PASSWORD: A6tP3osxByeM
      JS_ENABLE_PROFILING: "true"
      PYTHONUNBUFFERED: 1
    depends_on:
      - mds-db
    volumes:
      - js-rsa-vol:/app/rsa:rw
    ports:
      - 8002:8000

  annotation-service:
    image: registry.gitlab.com/empaia/services/annotation-service:0.16.3@sha256:69078a1090259062297aac0f34a6b920034d7f2e16596da1d456f74a543c182f
    command:
      - run.sh
      - --host=0.0.0.0
      - --workers=1
      - --port=8000
    environment:
      ANNOT_DB_HOST: mds-db
      ANNOT_DB_PORT: 5432
      ANNOT_DB: mds
      ANNOT_DB_USERNAME: empaia_test
      ANNOT_DB_PASSWORD: A6tP3osxByeM
      ANNOT_ROOT_PATH: /
      ANNOT_API_V1_INTEGRATION: annotation_service.api.v1.integrations.disable_auth:DisableAuth
      ANNOT_API_V3_INTEGRATION: annotation_service.api.v3.integrations.disable_auth:DisableAuth
      ANNOT_ITEM_LIMIT: 100000
      ANNOT_POST_LIMIT: 100000
      ANNOT_ENABLE_FILE_PROFILER: "true"
    depends_on:
      - mds-db
    ports:
      - 8003:8000

  marketplace-service-mock:
    image: registry.gitlab.com/empaia/service-mocks/marketplace-service-mock:0.1.52@sha256:657188113d5d025a16db1d0a6fbcaa5a6739e9c15a150b6f2f6374321082c7bd
    command:
      - uvicorn
      - --workers=1
      - --host=0.0.0.0
      - --port=8000
      - marketplace_service_mock.app:app
    environment:
      MPSM_DEBUG: "False"
      MPSM_API_INTEGRATION: marketplace_service_mock.api.integrations.disable_auth:DisableAuth
      MPSM_DISABLE_API_V0: "false"
      MPSM_ENABLE_API_V1: "true"
    volumes:
      - mps-mock-vol:/data
    ports:
      - 8004:8000

  simple-fastapi-server:
    build:
      context: ./app_service_performance_test/simple_fastapi_server
    ports:
      - 8005:8000

  # databases
  mds-db:
    image: registry.gitlab.com/empaia/integration/custom-pg:14
    command: postgres -c config_file=/etc/postgresql/postgres.conf
    environment:
      POSTGRES_DB: mds
      POSTGRES_USER: empaia_test
      POSTGRES_PASSWORD: A6tP3osxByeM
    volumes:
      - mds-db-vol:/var/lib/postgresql/data:rw

volumes:
  mds-db-vol:
    name: mds-db-vol
    external: false
  js-rsa-vol:
    name: js-rsa-vol
    external: false
  mps-mock-vol:
    name: no-auth-mps-mock-vol
    external: false
