import os
import time

import requests
from prettytable import MARKDOWN, PrettyTable

from app_service_performance_test.app import TestApp
from app_service_performance_test.compare import compare_to_latest_result


def get_download_max_in_kb_s():
    start = time.time()
    r = requests.get("http://127.0.0.1:8005/download")
    elapsed_time = time.time() - start
    return int(int(r.headers["content-length"]) / 1000 / elapsed_time)


def get_upload_max_in_kb_s():
    start = time.time()
    files = {"file": os.urandom(100_000_000)}
    start = time.time()
    r = requests.post("http://127.0.0.1:8005/upload", files=files)
    elapsed_time = time.time() - start
    return int(r.json()["file_size"] / 1000 / elapsed_time)


def get_latest_profiling_entry(service_port):
    r = requests.get(f"http://127.0.0.1:{service_port}/profiling")
    start_index = r.text.rfind('href="') + 6
    end_index = r.text.rfind('">JSON</a>')
    r = requests.get(f"http://127.0.0.1:{service_port}{r.text[start_index:end_index]}")
    profiling_all = r.json()
    return profiling_all


def get_profiling(elapsed_time):
    profiling_app_service = get_latest_profiling_entry(8000)
    profiling_medical_data_service = get_latest_profiling_entry(8001)
    profiling_annotation_service = get_latest_profiling_entry("8003/v3")
    app_time = round(elapsed_time - profiling_app_service["duration"], 2)
    app_service_time = round(
        profiling_app_service["duration"] - profiling_medical_data_service["duration"],
        2,
    )
    medical_data_service_time = round(
        profiling_medical_data_service["duration"]
        - profiling_annotation_service["duration"],
        2,
    )
    annotation_service_time = round(profiling_annotation_service["duration"], 2)
    return (
        app_time,
        app_service_time,
        medical_data_service_time,
        annotation_service_time,
    )


def get_profiling_finalize(elapsed_time):
    profiling_annotation_service = get_latest_profiling_entry("8003/v3")
    annotation_service_time = round(profiling_annotation_service["duration"], 2)
    other_services = round(elapsed_time - profiling_annotation_service["duration"], 2)
    return (annotation_service_time, other_services)


def create_html(table_extend_html, table_query_html, table_finalize_html):
    os.makedirs("public", exist_ok=True)
    with open("public/index.html", "w") as f:
        f.write(
            '<html><head><title>App Service Performance Test</title><link rel="stylesheet" href="style.css"></head><body>'
        )
        f.write("<h1>EXTEND COLLECTION</h1>")
        f.write(table_extend_html)
        f.write("<h1>QUERY</h1>")
        f.write(table_query_html)
        f.write("<h1>FINALIZE</h1>")
        f.write(table_finalize_html)
        f.write("</body></html>")


def main():
    # transfer speed baseline
    print("DOWNLOAD MAX", get_download_max_in_kb_s(), "KB/s")
    print("UPLOAD   MAX", get_upload_max_in_kb_s(), "KB/s")
    # test app service for different numbers of output point annotations
    field_names = [
        "Point Count",
        "Size Up in KB",
        "Size Down in KB",
        "Points/s",
        "Transfer Speed KB/s",
        "Time in s",
        "Time (App) in s",
        "Time (App Service) in s",
        "Time (Medical Data Service) in s",
        "Time (Annotation Service) in s",
    ]
    table_extend = PrettyTable(align="r")
    table_extend.field_names = field_names
    table_query = PrettyTable(align="r")
    table_query.field_names = field_names
    table_finalize = PrettyTable(align="r")
    table_finalize.field_names = [
        "Point Count",
        "Points/s",
        "Time in s",
        "Time (Annotation Service) in s",
        "Time (Other services) in s",
    ]
    for point_count in [10, 100, 1_000, 10_000]:
        test_app = TestApp()
        # EXTEND COLLECTION
        elapsed_time, size_kb_up, size_kb_down = test_app.add_points(point_count)
        (
            app_time,
            app_service_time,
            medical_data_service_time,
            annotation_service_time,
        ) = get_profiling(elapsed_time)
        table_extend.add_row(
            [
                point_count,
                size_kb_up,
                size_kb_down,
                int(point_count / elapsed_time),
                int((size_kb_up + size_kb_down) / elapsed_time),
                round(elapsed_time, 2),
                app_time,
                app_service_time,
                medical_data_service_time,
                annotation_service_time,
            ]
        )
        # GET COLLECTION
        start = time.time()
        test_app.get_collection()
        elapsed_time = time.time() - start
        (
            app_time,
            app_service_time,
            medical_data_service_time,
            annotation_service_time,
        ) = get_profiling(elapsed_time)
        table_query.add_row(
            [
                point_count,
                size_kb_up,
                size_kb_down,
                int(point_count / elapsed_time),
                int((size_kb_up + size_kb_down) / elapsed_time),
                round(elapsed_time, 2),
                app_time,
                app_service_time,
                medical_data_service_time,
                annotation_service_time,
            ]
        )
        # FINALIZE
        start = time.time()
        test_app.finalize()
        elapsed_time = time.time() - start
        (
            annotation_service_time,
            other_services_time,
        ) = get_profiling_finalize(elapsed_time)
        table_finalize.add_row(
            [
                point_count,
                int(point_count / elapsed_time),
                round(elapsed_time, 2),
                annotation_service_time,
                other_services_time,
            ]
        )
    print("EXTEND COLLECTION")
    print(table_extend)
    with open("extend_collection.json", "w") as f:
        f.write(table_extend.get_json_string())
    print("QUERY")
    print(table_query)
    with open("query.json", "w") as f:
        f.write(table_query.get_json_string())
    print("FINALIZE")
    print(table_finalize)
    with open("finalize.json", "w") as f:
        f.write(table_finalize.get_json_string())
    compare_to_latest_result()
    create_html(
        table_extend.get_html_string(),
        table_query.get_html_string(),
        table_finalize.get_html_string(),
    )


if __name__ == "__main__":
    main()
