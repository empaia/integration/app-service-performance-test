import json
import os
import sys
import time

import requests

from .helper import create_portal_app_and_running_job


class TestApp:
    def __init__(
        self,
        mds_host="http://127.0.0.1:8001",
        marketplace_host="http://127.0.0.1:8004",
        app_service_host="http://127.0.0.1:8000",
    ):
        self.marketplace_host = marketplace_host
        self.mds_host = mds_host
        self.app_service_host = app_service_host
        job_token = create_portal_app_and_running_job(
            io_spec={
                "my_wsi": {"type": "wsi"},
                "my_rectangles": {
                    "type": "collection",
                    "items": {"type": "rectangle", "reference": "io.my_wsi"},
                },
                "my_cells": {
                    "type": "collection",
                    "items": {
                        "type": "collection",
                        "reference": "io.my_rectangles.items",
                        "items": {"type": "point", "reference": "io.my_wsi"},
                    },
                },
            },
            modes_spec={
                "standalone": {
                    "inputs": ["my_wsi", "my_rectangles"],
                    "outputs": ["my_cells"],
                }
            },
            inputs={
                "my_wsi": {
                    "type": "wsi",
                    "slide_id": "8d32dba05a4558218880f06caf30d3ac",
                },
                "my_rectangles": {
                    "name": "My Rectangles",
                    "type": "collection",
                    "item_type": "rectangle",
                    "items": [
                        {
                            "name": "Annotation Name",
                            "description": "Annotation Description",
                            "npp_created": 499,
                            "npp_viewing": [499, 7984],
                            "upper_left": [1000, 2000],
                            "width": 300,
                            "height": 500,
                            "type": "rectangle",
                            "reference_id": "8d32dba05a4558218880f06caf30d3ac",
                            "reference_type": "wsi",
                        }
                    ],
                },
            },
        )
        self.job_id = job_token["job_id"]
        self._prepare_collection_items_output()

    def add_points(self, number_of_points):
        item = {
            "name": "cell",
            "type": "point",
            "reference_id": "8d32dba05a4558218880f06caf30d3ac",
            "reference_type": "wsi",
            "npp_created": 499,
            "npp_viewing": [499, 7984],
            "coordinates": [100, 200],
            "creator_id": self.job_id,
            "creator_type": "job",
        }

        data = {"items": [item for _ in range(number_of_points)]}
        data_json = json.dumps(data)
        size_kb_up = int(sys.getsizeof(data_json) / 1024)
        start = time.time()
        r = requests.post(
            f"{self.app_service_host}/v3/{self.job_id}/collections/{self.point_collection_id}/items",
            data=data_json,
        )
        elapsed_time = time.time() - start
        r.raise_for_status()
        size_kb_down = int(sys.getsizeof(json.dumps(r.json())) / 1024)
        return elapsed_time, size_kb_up, size_kb_down

    def get_collection(self):
        data = {
            "references": ["8d32dba05a4558218880f06caf30d3ac"],
            "viewport": {"x": 0, "y": 0, "width": 1000, "height": 1000},
        }
        r = requests.put(
            f"{self.app_service_host}/v3/{self.job_id}/collections/{self.point_collection_id}/query",
            data=json.dumps(data),
        )
        r.raise_for_status()

    def finalize(self):
        r = requests.put(f"{self.app_service_host}/v3/{self.job_id}/finalize")
        r.raise_for_status()

    def _prepare_collection_items_output(self):
        r = requests.get(
            f"{self.app_service_host}/v3/{self.job_id}/inputs/my_rectangles"
        )
        r.raise_for_status()
        self.my_rectangles = r.json()
        data = {
            "name": "My Cells",
            "item_type": "collection",
            "creator_id": self.job_id,
            "creator_type": "job",
            "type": "collection",
            "items": []
        }
        r = requests.post(
            f"{self.app_service_host}/v3/{self.job_id}/outputs/my_cells",
            json=data
        )
        r.raise_for_status()
        my_cells = r.json()
        data = {
            "items": [
                {
                    "name": "My Points",
                    "item_type": "point",
                    "creator_id": self.job_id,
                    "creator_type": "job",
                    "reference_id": self.my_rectangles["items"][0]["id"],
                    "reference_type": "annotation",
                    "type": "collection",
                    "items": []
                }
            ]
        }
        r = requests.post(
            f"{self.app_service_host}/v3/{self.job_id}/collections/{my_cells['id']}/items",
            json=data,
        )
        r.raise_for_status()
        self.point_collection_id = r.json()["items"][0]["id"]
