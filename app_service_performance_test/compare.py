import json
import os

import requests


def get_latest_results(project_id, token):
    r = requests.get(
        f"https://gitlab.com/api/v4/projects/{project_id}/jobs?access_token={token}"
    )
    jobs = r.json()
    for job in jobs:
        if (
            job["ref"] == "main"
            and job["name"] == "performance-test"
            and job["status"] == "success"
        ):
            job_id = job["id"]
            break
    r = requests.get(
        f"https://gitlab.com/api/v4/projects/{project_id}/jobs/{job_id}/artifacts/extend_collection.json?access_token={token}"
    )
    extend_collection_reference = r.json()[-1]
    r = requests.get(
        f"https://gitlab.com/api/v4/projects/{project_id}/jobs/{job_id}/artifacts/query.json?access_token={token}"
    )
    query_reference = r.json()[-1]
    r = requests.get(
        f"https://gitlab.com/api/v4/projects/{project_id}/jobs/{job_id}/artifacts/finalize.json?access_token={token}"
    )
    finalize_reference = r.json()[-1]
    return extend_collection_reference, query_reference, finalize_reference


def compare_to_latest_result():
    project_id = os.getenv("CI_PROJECT_ID")
    token = os.getenv("GITLAB_ACCESS_TOKEN")
    if project_id and token:
        (
            extend_collection_reference,
            query_reference,
            finalize_reference,
        ) = get_latest_results(project_id, token)
        with open("extend_collection.json") as f:
            extend_collection = json.load(f)[-1]
        with open("query.json") as f:
            query = json.load(f)[-1]
        with open("finalize.json") as f:
            finalize = json.load(f)[-1]
        print("EXTEND COLLECTION")
        print("CURRENT TIME:", extend_collection["Time in s"])
        print("REFERNECE TIME:", extend_collection_reference["Time in s"])
        assert (
            extend_collection["Time in s"]
            < 1.2 * extend_collection_reference["Time in s"]
        )
        print("QUERY")
        print("CURRENT TIME:", query["Time in s"])
        print("REFERNECE TIME:", query_reference["Time in s"])
        assert query["Time in s"] < 1.2 * query_reference["Time in s"]
        print("FINALIZE")
        print("CURRENT TIME:", finalize["Time in s"])
        print("REFERNECE TIME:", finalize_reference["Time in s"])
        assert finalize["Time in s"] < 1.2 * finalize_reference["Time in s"]
