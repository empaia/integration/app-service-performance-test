# App Service Performance Test

The performance of the App Service and underlying services (such as Medical Data Service and Annotation Service) is evaluated with respect to a large set of point annotations. The current results can be seen [here](https://empaia.gitlab.io/integration/app-service-performance-test/).

## Quickstart

Check out this repository and run

```shell
poetry install
docker-compose up -d
poetry run python3 app_service_performance_test/main.py
```

This should start all services and run the performance test evaluating:

* the extension of a collection by a different number of point annotations
* querying a different number of point annotations
* locking a different number of point annotations by finalizing the app

The results can be seen in the console.
